# Stephane Verville-Vohl et Remi Langevin
import sys

def parse(line, el_type):
    line = line.strip()
    buff = ""
    elems = []
    splitted = line.split("|")
    for i,elem in enumerate(splitted):
        tmp = splitted[i].split("=")
        elems.append((tmp[0], tmp[1].replace('"','')))
    # start elem
    buff += "  <{}>\n".format(el_type)
    # put content
    for e in elems:
        if e[0] != "nf":
            buff += "    <{0}>{1}</{0}>\n".format(*e)
    # end elem
    buff += "  </{}>\n".format(el_type)
    return buff

def toXML(in_file,out):
    with open(in_file,'r',encoding='ISO-8859-1') as f:
        for line in f:
            if line.startswith("eid"):
                out.write(parse(line,"node"))
            elif line.startswith("rid"):
                out.write(parse(line,"rel"))


def add_rel_types(in_file, out):
    start = "// ---- Relation stats"
    end= "// ---- Node stats"
    rel_list = list_primer(in_file, start, end)
    rel_types = []

    for i, relation in enumerate(rel_list):
        tmp = rel_list[i].split(" ")
        intermediate = [tmp[5], tmp[6].split("=")[1]]
        rel_types.append(intermediate)
    
    for r in rel_types:
        out.write('    <rtype t="{1}">{0}</rtype>\n'.format(*r))
    out.write('\n')


def list_primer(in_file, start, end):
    f = open(in_file, "r", encoding='ISO-8859-1')
    data = f.read()

    start_index = data.find(start) + len(start)
    end_index= data.find(end)

    chunk = data[start_index:end_index]
    chunk = chunk.strip()

    type_list = chunk.split("//")
    type_list = type_list[1:]

    return type_list


def prolog():
    return '<?xml version="1.0" encoding="utf-8"?>\n\
            <!DOCTYPE data [\n\
            <!ENTITY nbsp   "&#160;">\n\
            <!ENTITY iexcl  "&#161;">\n\
            <!ENTITY cent   "&#162;">\n\
            <!ENTITY pound  "&#163;">\n\
            <!ENTITY curren "&#164;">\n\
            <!ENTITY yen    "&#165;">\n\
            <!ENTITY brvbar "&#166;">\n\
            <!ENTITY sect   "&#167;">\n\
            <!ENTITY uml    "&#168;">\n\
            <!ENTITY copy   "&#169;">\n\
            <!ENTITY ordf   "&#170;">\n\
            <!ENTITY laquo  "&#171;">\n\
            <!ENTITY not    "&#172;">\n\
            <!ENTITY shy    "&#173;">\n\
            <!ENTITY reg    "&#174;">\n\
            <!ENTITY macr   "&#175;">\n\
            <!ENTITY deg    "&#176;">\n\
            <!ENTITY plusmn "&#177;">\n\
            <!ENTITY sup2   "&#178;">\n\
            <!ENTITY sup3   "&#179;">\n\
            <!ENTITY acute  "&#180;">\n\
            <!ENTITY micro  "&#181;">\n\
            <!ENTITY para   "&#182;">\n\
            <!ENTITY middot "&#183;">\n\
            <!ENTITY cedil  "&#184;">\n\
            <!ENTITY sup1   "&#185;">\n\
            <!ENTITY ordm   "&#186;">\n\
            <!ENTITY raquo  "&#187;">\n\
            <!ENTITY frac14 "&#188;">\n\
            <!ENTITY frac12 "&#189;">\n\
            <!ENTITY frac34 "&#190;">\n\
            <!ENTITY iquest "&#191;">\n\
            <!ENTITY Agrave "&#192;">\n\
            <!ENTITY Aacute "&#193;">\n\
            <!ENTITY Acirc  "&#194;">\n\
            <!ENTITY Atilde "&#195;">\n\
            <!ENTITY Auml   "&#196;">\n\
            <!ENTITY Aring  "&#197;">\n\
            <!ENTITY AElig  "&#198;">\n\
            <!ENTITY Ccedil "&#199;">\n\
            <!ENTITY Egrave "&#200;">\n\
            <!ENTITY Eacute "&#201;">\n\
            <!ENTITY Ecirc  "&#202;">\n\
            <!ENTITY Euml   "&#203;">\n\
            <!ENTITY Igrave "&#204;">\n\
            <!ENTITY Iacute "&#205;">\n\
            <!ENTITY Icirc  "&#206;">\n\
            <!ENTITY Iuml   "&#207;">\n\
            <!ENTITY ETH    "&#208;">\n\
            <!ENTITY Ntilde "&#209;">\n\
            <!ENTITY Ograve "&#210;">\n\
            <!ENTITY Oacute "&#211;">\n\
            <!ENTITY Ocirc  "&#212;">\n\
            <!ENTITY Otilde "&#213;">\n\
            <!ENTITY Ouml   "&#214;">\n\
            <!ENTITY times  "&#215;">\n\
            <!ENTITY Oslash "&#216;">\n\
            <!ENTITY Ugrave "&#217;">\n\
            <!ENTITY Uacute "&#218;">\n\
            <!ENTITY Ucirc  "&#219;">\n\
            <!ENTITY Uuml   "&#220;">\n\
            <!ENTITY Yacute "&#221;">\n\
            <!ENTITY THORN  "&#222;">\n\
            <!ENTITY szlig  "&#223;">\n\
            <!ENTITY agrave "&#224;">\n\
            <!ENTITY aacute "&#225;">\n\
            <!ENTITY acirc  "&#226;">\n\
            <!ENTITY atilde "&#227;">\n\
            <!ENTITY auml   "&#228;">\n\
            <!ENTITY aring  "&#229;">\n\
            <!ENTITY aelig  "&#230;">\n\
            <!ENTITY ccedil "&#231;">\n\
            <!ENTITY egrave "&#232;">\n\
            <!ENTITY eacute "&#233;">\n\
            <!ENTITY ecirc  "&#234;">\n\
            <!ENTITY euml   "&#235;">\n\
            <!ENTITY igrave "&#236;">\n\
            <!ENTITY iacute "&#237;">\n\
            <!ENTITY icirc  "&#238;">\n\
            <!ENTITY iuml   "&#239;">\n\
            <!ENTITY eth    "&#240;">\n\
            <!ENTITY ntilde "&#241;">\n\
            <!ENTITY ograve "&#242;">\n\
            <!ENTITY oacute "&#243;">\n\
            <!ENTITY ocirc  "&#244;">\n\
            <!ENTITY otilde "&#245;">\n\
            <!ENTITY ouml   "&#246;">\n\
            <!ENTITY divide "&#247;">\n\
            <!ENTITY oslash "&#248;">\n\
            <!ENTITY ugrave "&#249;">\n\
            <!ENTITY uacute "&#250;">\n\
            <!ENTITY ucirc  "&#251;">\n\
            <!ENTITY uuml   "&#252;">\n\
            <!ENTITY yacute "&#253;">\n\
            <!ENTITY thorn  "&#254;">\n\
            <!ENTITY yuml   "&#255;">\n\
            ]>\n\
            \n<data xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"\
               xsi:noNamespaceSchemaLocation="rezo.xsd">\n'

    '''
    Elegant way would have been, but....

    return '<?xml version="1.0" encoding="utf-8"?>\n\
            <!DOCTYPE data [\n\
                <!ENTITY % HTMLlat1 PUBLIC\n\
                   "-//W3C//ENTITIES Latin 1 for XHTML//EN"\n\
                   "http://www.w3.org/TR/xhtml1/DTD/xhtml-lat1.ent">\n\
                %HTMLlat1;\n\
            ]>\n\
            \n<data>\n'
    '''



def epilog():
    return "</data>"

def do_it(in_file,out_file):
    with open(out_file, 'w') as out:
        out.write(prolog())
        add_rel_types(in_file, out)
        toXML(in_file,out)
        out.write(epilog())

if __name__=="__main__":
    try:
        in_file,out_file = sys.argv[1],sys.argv[2]
        do_it(in_file,out_file)
    except IndexError:
        print("Usage: python toXML.py {in_file} {out_file}")
        
