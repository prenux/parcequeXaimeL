<?xml version="1.0" encoding="utf-8"?>
<!--Stephane Verville-Vohl et Remi Langevin-->
<xsl:stylesheet version="2.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:exsl="http://exslt.org/common"
    extension-element-prefixes="exsl">
    <xsl:variable name="datafile" select="document('rezo.xml')"/>

<!-- Program root -->
<xsl:template match="script">
    <xsl:text disable-output-escaping='yes'>&lt;!DOCTYPE html&gt;</xsl:text>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
            <link rel="stylesheet" href="tp1.css"/>
            <title>Script</title>
        </head>
        <body>
            <xsl:apply-templates/>
        </body>
    </html>
</xsl:template>


<!--============================================================-->
<!--======================== COMMENT ===========================-->
<!--============================================================-->

<xsl:template match="comment">
    <div class="comment">
        <xsl:value-of select="."/>
    </div>
</xsl:template>


<!--============================================================-->
<!--====================== LINKEDFROM ==========================-->
<!--============================================================-->

<xsl:template match="linkedFrom[not(@rel)]">	    
    <xsl:call-template name="lfrom">
        <xsl:with-param name="target" select="text()"/>
        <xsl:with-param name="depth">
            <xsl:choose>
                <xsl:when test="@prof">
                    <xsl:value-of select="@prof"/>
                </xsl:when>
                <xsl:otherwise>1</xsl:otherwise>
            </xsl:choose>
        </xsl:with-param>
    </xsl:call-template>
</xsl:template>


<xsl:template name="lfrom">
    <xsl:param name="target"/>
    <xsl:param name="depth"/>
    <xsl:variable name="linkedid" select="$datafile/data/node[n = $target]/eid"/>
    <xsl:variable name="rel_list" select="distinct-values($datafile/data/rel[n1 = $linkedid]/t)"/>

    <div>
        <xsl:attribute name="class">level<xsl:value-of select="$depth"/></xsl:attribute>
        <span class="source linked"><xsl:value-of select="$target"/></span>
        <dl>
            <xsl:for-each select="$rel_list">
                <dt><xsl:value-of select="$datafile/data/rtype[current() = @t]"/></dt>
                <dd>
                    <xsl:call-template name="scour">
                        <xsl:with-param name="reltype" select="."/> 
                        <xsl:with-param name="id" select="$linkedid"/>
                        <xsl:with-param name="depth" select="$depth"/>
                    </xsl:call-template>
                </dd>
            </xsl:for-each>
        </dl>
    </div>
</xsl:template>


<xsl:template name="scour">
    <xsl:param name="reltype"/>
    <xsl:param name="id"/>
    <xsl:param name="depth"/>
    <xsl:for-each select="$datafile/data/rel[$id = n1]">
        <xsl:if test="t = $reltype">
            <span class="word">
                <xsl:value-of select="$datafile/data/node[eid = current()/n2]/n"/>
            </span>
        </xsl:if>
    </xsl:for-each>

    <xsl:if test="$depth &gt; 1">
        <xsl:for-each select="$datafile/data/rel[$id = n1]">
            <xsl:if test="t = $reltype">
                <xsl:call-template name="lfrom">
                    <xsl:with-param name="target" select="$datafile/data/node[eid = current()/n2]/n"/>
                    <xsl:with-param name="depth" select="$depth - 1"/>
                </xsl:call-template>
            </xsl:if>
        </xsl:for-each>
    </xsl:if>
</xsl:template>


<!-- linkedFrom avec specification de relation -->
<xsl:template match="linkedfrom[@rel]">
    <xsl:call-template name="lfromwithrel">
        <xsl:with-param name="target" select="text()"/>
        <xsl:with-param name="depth">
            <xsl:choose>
                <xsl:when test="@prof">
                    <xsl:value-of select="@prof"/>
                </xsl:when>
                <xsl:otherwise>1</xsl:otherwise>
            </xsl:choose>
        </xsl:with-param>
        <xsl:with-param name="rel" select="@rel"/>
    </xsl:call-template>
</xsl:template>


<xsl:template name="lfromwithrel">
    <xsl:param name="target"/>
    <xsl:param name="depth"/>
    <xsl:param name="rel"/>
    <xsl:variable name="lidwrel" select="$datafile/data/node[n = $target]/eid"/>

    <div>
        <xsl:attribute name="class">level<xsl:value-of select="$depth"/></xsl:attribute>
        <span class="source linked"><xsl:value-of select="$target"/></span>
        <dl>
            <dt><xsl:value-of select="$rel"/></dt>
            <dd>
                <xsl:call-template name="scourwithrel">
                    <xsl:with-param name="reltype" select="$rel"/> 
                    <xsl:with-param name="id" select="$lidwrel"/>
                    <xsl:with-param name="depth" select="$depth"/>
                </xsl:call-template>
            </dd>
        </dl>
    </div>
</xsl:template>


<xsl:template name="scourwithrel">
    <xsl:param name="reltype"/>
    <xsl:param name="id"/>
    <xsl:param name="depth"/>
    <xsl:for-each select="$datafile/data/rel[$id = n1]">
        <xsl:if test="t = $reltype">
            <span class="word">
                <xsl:value-of select="$datafile/data/node[eid = current()/n2]/n"/>
            </span>
        </xsl:if>
    </xsl:for-each>

    <xsl:if test="$depth &gt; 1">
        <xsl:for-each select="$datafile/data/rel[$id = n1]">
            <xsl:if test="t = $reltype">
                <xsl:call-template name="lfromwithrel">
                    <xsl:with-param name="target" select="$datafile/data/node[eid = current()/n2]/n"/>
                    <xsl:with-param name="depth" select="$depth - 1"/>
                    <xsl:with-param name="rel" select="$reltype"/>
                </xsl:call-template>
            </xsl:if>
        </xsl:for-each>
    </xsl:if>
</xsl:template>    


<!--============================================================-->
<!--====================== STARTWITH ===========================-->
<!--============================================================-->

<xsl:template match="startWith">
    <div class="startWith">
        <ul class="startWith">
            <xsl:variable name="needle" select="text()"/>
            <xsl:for-each select="$datafile/data/node">
                <xsl:if test="starts-with(n,$needle)">
                    <li><xsl:value-of select="n"/></li>
                </xsl:if>
            </xsl:for-each>
        </ul>
    </div>
</xsl:template>


<!--============================================================-->
<!--====================== DEGREEOUT ===========================-->
<!--============================================================-->

<xsl:template match="degreeOut">
    <xsl:variable name="ntop">
        <xsl:choose>
            <xsl:when test="@nb">
                <xsl:value-of select="@nb"/>
            </xsl:when>
            <xsl:otherwise>10</xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <div class="degreeOut">
        <table class="degreeOut">
            <xsl:for-each select="$datafile/data/node/eid">
                <xsl:sort order="descending" select="count($datafile/data/rel[n2 = current()])"/>
                <xsl:if test="position() &lt;= $ntop">
                    <tr><td><xsl:value-of select="../n"/></td><td><xsl:value-of select="count($datafile/data/rel[n2 = current()])"/></td></tr>
                </xsl:if>
            </xsl:for-each>
        </table>
    </div>
</xsl:template>


<!--============================================================-->
<!--======================= DEGREEIN ===========================-->
<!--============================================================-->

<xsl:template match="degreeIn">
    <xsl:variable name="ntop">
        <xsl:choose>
            <xsl:when test="@nb">
                <xsl:value-of select="@nb"/>
            </xsl:when>
            <xsl:otherwise>10</xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <div class="degreeIn">
        <table class="degreeIn">
            <xsl:for-each select="$datafile/data/node/eid">
                <xsl:sort order="descending" select="count($datafile/data/rel[n1=current()])"/>
                <xsl:if test="position() &lt;= $ntop">
                    <tr><td><xsl:value-of select="../n"/></td><td><xsl:value-of select="count($datafile/data/rel[n1 = current()])"/></td></tr>
                </xsl:if>
            </xsl:for-each>
        </table>
    </div>
</xsl:template>


<!--============================================================-->
<!--======================== NBOFTHINGS ========================-->
<!--============================================================-->

<xsl:template match="nbNodes">
    <div class="result nbNodes">
        Nombre de noeuds: <xsl:value-of select="count($datafile/data/node)"/>
    </div>
</xsl:template>


<!-- nbRelations -->
<xsl:template match="nbRelations">
    <div class="result nbRelations">
        Nombre de relations: <xsl:value-of select="count($datafile/data/rel)"/>
    </div>
</xsl:template>


<!-- nbRelationTypes -->
<xsl:template match="nbRelationTypes">
    <div class="result nbRelationTypes">
        Nombre de types de relations: <xsl:value-of select="count($datafile/data/rtype)"/>
    </div>
</xsl:template>


<!--============================================================-->
<!--======================== CHAINE ============================-->
<!--============================================================-->

<xsl:template match="chaine">
    <xsl:variable name="content" select="text()"/>
    <xsl:variable name="depth" select="@prof"/>
    <xsl:variable name="chaineid" select="$datafile/data/node[n = $content]/eid"/>
    <xsl:variable name="acc">
    </xsl:variable>
    <div class="chaines">
        <xsl:call-template name="chaine">
            <xsl:with-param name="acc" select="$acc"/>
            <xsl:with-param name="depth" select="$depth"/>
            <xsl:with-param name="chaineid" select="$chaineid"/>
        </xsl:call-template>
    </div>
</xsl:template>

<!-- Recursive template for chaine -->
<xsl:template name="chaine">
    <xsl:param name="acc"/>
    <xsl:param name="depth"/>
    <xsl:param name="chaineid"/>
    <xsl:choose>
        <xsl:when test="$depth = 1">
            <div class="chaine">
                <xsl:copy-of select="$acc"/>
                <span>
                    <xsl:value-of select="$datafile/data/node[eid=$chaineid]/n"/>
                </span>
            </div>
        </xsl:when>
        <xsl:otherwise>
            <xsl:for-each select="$datafile/data/rel[n1 = $chaineid]/n2">
                <xsl:variable name="new_eid" select="text()"/>
                <xsl:variable name="new_acc">
                    <xsl:copy-of select="$acc"/>
                    <span>
                        <xsl:value-of select="$datafile/data/node[eid=$chaineid]/n/text()"/>
                    </span>
                </xsl:variable>
                <xsl:call-template name="chaine">
                    <xsl:with-param name="acc" select="$new_acc"/>
                    <xsl:with-param name="depth" select="$depth - 1"/>
                    <xsl:with-param name="chaineid" select="$new_eid"/>
                </xsl:call-template>
            </xsl:for-each>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<!--============================================================-->
<!--======================= BAGOFWORDS =========================-->
<!--============================================================-->

<xsl:template match="bagOfWords">
    <xsl:variable name="title" select="text()"/>
    <xsl:variable name="prof">
        <xsl:choose>
            <xsl:when test="@prof">	
                <xsl:value-of select="@prof"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="1"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>

    <div class="result bagOfWords">
        <span class="bow title"><xsl:value-of select="$title"/></span>
        <br/>
        <xsl:call-template name="bowgenerate">
            <xsl:with-param name="target" select="$title"/>
            <xsl:with-param name="depth" select="$prof"/>
        </xsl:call-template>
        <br/>
    </div>
</xsl:template>


<!-- appel recursif pour bagOfWords -->
<xsl:template name="bowgenerate">
    <xsl:param name="target"/>
    <xsl:param name="depth"/>

    <xsl:variable name="bowid" select="$datafile/data/node[n=$target]/eid"/>
    <xsl:for-each select="$datafile/data/rel[n1 = $bowid]/n2">
        <span class="word"><xsl:value-of select="$datafile/data/node[eid = current()]/n"/></span>
    </xsl:for-each>

    <xsl:if test="$depth &gt; 1">
        <xsl:for-each select="$datafile/data/rel[n1 = $bowid]/n2">
            <xsl:call-template name="bowgenerate">
                <xsl:with-param name="target" select="$datafile/data/node[eid = current()]/n"/>
                <xsl:with-param name="depth" select="$depth - 1"/>
            </xsl:call-template>
        </xsl:for-each>
    </xsl:if>
</xsl:template>

</xsl:stylesheet>

